Haskell Programming
===================
from first principles

My solutions working my way through the book [Haskell Programming from first principles](http://haskellbook.com).

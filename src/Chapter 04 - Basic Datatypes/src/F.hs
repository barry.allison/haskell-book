module F where

f :: (a, b) -> (c, d) -> ((b, d), (a, c))
--f x y = ((snd x, snd y), (fst x, fst y))
f x y = ((b, d), (a, c))
    where a = fst x
          b = snd x
          c = fst y
          d = snd y

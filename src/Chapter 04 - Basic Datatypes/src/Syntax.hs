module Syntax where

x = (+)
f xs = w `x` 1
    where w = length xs


myId = \ x -> x


myHead xs = \ (x:xs) -> x


myFst (a, b) = a

module Print3 where

myGreeting :: String
myGreeting = "Hello, " ++ "worms!"

hello :: String
hello = "Hello,"

world :: String
world = "worms!"

main :: IO ()
main = do
  putStrLn myGreeting
  putStrLn secondGreeting
  where secondGreeting = concat [hello, " ", world]

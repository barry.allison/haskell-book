module Curry where

exclaim :: String -> String
exclaim x = x ++ "!"

fourth :: String -> String
fourth x = [x !! 4]

dropNine :: String -> String
dropNine x = drop 9 x

curryIsAwesome :: String
curryIsAwesome = "Curry is awesome"

thirdLetter :: String -> Char
thirdLetter x = x !! 2

letterIndex :: Int -> Char
letterIndex x = curryIsAwesome !! x

rvrs :: String -> String
rvrs x = drop 9 x ++ take 4 (drop 5 x) ++ take 5 x

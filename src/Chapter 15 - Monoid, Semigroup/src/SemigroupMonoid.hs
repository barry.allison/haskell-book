module SemigroupMonoid where

import           Data.Monoid     (Monoid, Sum (..))
import           Data.Semigroup  (Semigroup, (<>))
import           MonoidSpec      (monoidLeftIdentity, monoidRightIdentity,
                                  semigroupAssoc)
import           Test.Hspec
import           Test.QuickCheck hiding (Failure, Success)

-------------------------------------------------------------------------------
--                            Semigroup exercises                            --
-------------------------------------------------------------------------------

-- Exercise 1.
data Trivial = Trivial deriving (Eq, Show)
type TrivialAssoc = Trivial -> Trivial -> Trivial -> Bool

instance Semigroup Trivial where
    _ <> _ = Trivial

instance Arbitrary Trivial where
    arbitrary = return Trivial

-- Exercise 2.
newtype Identity a = Identity a deriving (Eq, Show)
type IdTest = Identity String
type IdentityAssoc =  IdTest -> IdTest -> IdTest -> Bool

instance Semigroup a => Semigroup (Identity a) where
    (Identity x) <> (Identity y) = Identity (x <> y)

instance Arbitrary a => Arbitrary (Identity a) where
    arbitrary = do
      a <- arbitrary
      return (Identity a)

-- Exercise 3
data Two a b = Two a b deriving (Eq, Show)
type TwoTest = Two String [Int]
type TwoAssoc =  TwoTest -> TwoTest -> TwoTest -> Bool

instance (Semigroup a, Semigroup b) => Semigroup (Two a b) where
    (Two a b) <> (Two a' b') = Two (a <> a') (b <> b')

instance (Arbitrary a, Arbitrary b) => Arbitrary (Two a b) where
    arbitrary = do
      a <- arbitrary
      b <- arbitrary
      return (Two a b)

-- Exercise 4
data Three a b c = Three a b c deriving (Eq, Show)
type ThreeTest = Three String (Sum Integer) Ordering
type ThreeAssoc =  ThreeTest -> ThreeTest -> ThreeTest -> Bool

instance (Semigroup a, Semigroup b, Semigroup c) => Semigroup (Three a b c) where
    (Three a b c) <> (Three a' b' c') = Three (a <> a') (b <> b') (c <> c')

instance (Arbitrary a, Arbitrary b, Arbitrary c) => Arbitrary (Three a b c) where
    arbitrary = do
      a <- arbitrary
      b <- arbitrary
      c <- arbitrary
      return (Three a b c)

-- Exercise 5
data Four a b c d = Four a b c d deriving (Eq, Show)
type FourTest = Four String [Int] Ordering (Sum Int)
type FourAssoc =  FourTest -> FourTest -> FourTest -> Bool

instance (Semigroup a, Semigroup b, Semigroup c, Semigroup d) => Semigroup (Four a b c d) where
    (Four a b c d) <> (Four a' b' c' d') = Four (a <> a') (b <> b') (c <> c') (d <> d')

instance (Arbitrary a, Arbitrary b, Arbitrary c, Arbitrary d) => Arbitrary (Four a b c d) where
    arbitrary = do
      a <- arbitrary
      b <- arbitrary
      c <- arbitrary
      d <- arbitrary
      return (Four a b c d)

-- Exercise 6.
newtype BoolConj = BoolConj Bool deriving (Eq, Show)
type BoolConjAssoc = BoolConj -> BoolConj -> BoolConj -> Bool

instance Semigroup BoolConj where
    (BoolConj True) <> (BoolConj True) = BoolConj True
    _               <> _               = BoolConj False

instance Arbitrary BoolConj where
    arbitrary = do
      a <- arbitrary
      return (BoolConj a)

-- Exercise 7.
newtype BoolDisj = BoolDisj Bool deriving (Eq, Show)
type BoolDisjAssoc =  BoolDisj -> BoolDisj -> BoolDisj -> Bool

instance Semigroup BoolDisj where
    (BoolDisj False) <> (BoolDisj False) = BoolDisj False
    _               <> _               = BoolDisj True

instance Arbitrary BoolDisj where
    arbitrary = do
      a <- arbitrary
      return (BoolDisj a)

-- Exercise 8.
data Or a b = Fst a | Snd b deriving (Eq, Show)
type OrTest = Or Char String
type OrAssoc =  OrTest -> OrTest -> OrTest -> Bool

instance Semigroup (Or a b) where
    Fst _ <> b = b
    a     <> _ = a

genOr :: (Arbitrary a, Arbitrary b) => Gen (Or a b)
genOr = do
  a <- arbitrary
  b <- arbitrary
  elements [Fst a, Snd b]

instance (Arbitrary a, Arbitrary b) => Arbitrary (Or a b) where
    arbitrary = genOr

-- Exercise 9.
newtype Combine a b = Combine { unCombine :: (a -> b) }
type CombAssoc =  Combine String Int
               -> Combine String Int
               -> Combine String Int
               -> Bool

-- instance Show (Combine a b) where
--     show _ = "(a -> b)"

instance (Semigroup b) => Semigroup (Combine a b) where
   (Combine f) <> (Combine g) = Combine (\x -> f x <> g x)

-- We need to generate a function a -> b for Combine
-- The book mentions using CoArbitrary to generate a function a -> b.
-- Since I don't understand coparbitrary, but whoever wriote QuickCheck does,
-- let QuickCheck do it.
genFunAB :: (CoArbitrary a, Arbitrary b) => Gen (a -> b)
genFunAB = arbitrary

-- We can generate an a -> b now so:
genCombine :: (CoArbitrary a, Arbitrary b) => Gen (Combine a b)
genCombine = do
  a <- genFunAB
  return (Combine a)

instance (CoArbitrary a, Arbitrary b) => Arbitrary (Combine a b) where
    arbitrary = genCombine

-- Can't actually use QuickCheck to test because semigroupAssoc needs an Eq instance for (Combine a b)

-- Exercise 10.
-- This is just automatic with no understanding and no way to test it.
newtype Comp a = Comp { unComp :: (a -> a) }
type CompAssoc =  Comp String
               -> Comp String
               -> Comp String
               -> Bool

instance (Semigroup a) => Semigroup (Comp a) where
    (Comp f) <> (Comp g) = Comp (f . g)

genFunAA :: (CoArbitrary a, Arbitrary a) => Gen (a -> a)
genFunAA = arbitrary

genComp :: (CoArbitrary a, Arbitrary a) => Gen (Comp a)
genComp = do
  a <- genFunAA
  return (Comp a)

instance (CoArbitrary a, Arbitrary a) => Arbitrary (Comp a) where
    arbitrary = genComp
-- Again stuck because no idea how to write an Eq instance for (Comp a)

-- Exercise 11.
data AccumulateRight a b = AccumulateRight (Validation a b)
                           deriving (Eq, Show)
type AccumulateRightAssoc =  AccumulateRight Ordering [Int]
                          -> AccumulateRight Ordering [Int]
                          -> AccumulateRight Ordering [Int]
                          -> Bool

data Validation a b = Failure a
                    | Success b
                      deriving (Eq, Show)
type ValidationAssoc =  Validation String [Int]
                     -> Validation String [Int]
                     -> Validation String [Int]
                     -> Bool

instance Semigroup a => Semigroup (Validation a b) where
    Failure a <> Failure b = Failure (a <> b)
    Failure a <> Success b = Failure a
    Success _ <> Failure b = Failure b
    Success a <> _         = Success a

genValidation :: (Arbitrary a, Arbitrary b) => Gen (Validation a b)
genValidation = do
  a <- arbitrary
  b <- arbitrary
  elements [Failure a, Success b]

instance (Arbitrary a, Arbitrary b) => Arbitrary (Validation a b) where
    arbitrary = genValidation

-- Exercise 12.
data AccumulateBoth a b =
    AccumulateBoth (Validation a b)
    deriving (Eq, Show)
type AccumulateBothAssoc =  AccumulateBoth Ordering String
                         -> AccumulateBoth Ordering String
                         -> AccumulateBoth Ordering String
                         -> Bool

instance Semigroup b => Semigroup (AccumulateRight a b) where
    AccumulateRight (Success a) <> AccumulateRight (Success b) = AccumulateRight (Success (a <> b))
    AccumulateRight (Success a) <> _                           = AccumulateRight (Success a)
    _                           <> b                           = b

genAccumulateRight :: (Arbitrary a, Arbitrary b) => Gen (AccumulateRight a b)
genAccumulateRight = do
  a <- arbitrary
  b <- arbitrary
  elements [AccumulateRight (Failure a), AccumulateRight (Success b)]

instance (Arbitrary a, Arbitrary b) => Arbitrary (AccumulateRight a b) where
    arbitrary = genAccumulateRight

-- Exercise 13.
instance (Semigroup a, Semigroup b) => Semigroup (AccumulateBoth a b) where
    AccumulateBoth (Failure a) <> AccumulateBoth (Failure b) = AccumulateBoth (Failure (a <> b))
    AccumulateBoth (Success a) <> AccumulateBoth (Success b) = AccumulateBoth (Success (a <> b))
    AccumulateBoth (Failure a) <> _                          = AccumulateBoth (Failure a)
    _                          <> AccumulateBoth (Failure b) = AccumulateBoth (Failure b)

genAccumulateBoth :: (Arbitrary a, Arbitrary b) => Gen (AccumulateBoth a b)
genAccumulateBoth = do
  a <- arbitrary
  b <- arbitrary
  elements [AccumulateBoth (Failure a), AccumulateBoth (Success b)]

instance (Arbitrary a, Arbitrary b) => Arbitrary (AccumulateBoth a b) where
    arbitrary = genAccumulateBoth

-------------------------------------------------------------------------------
--                         Test Semigroup exercises                          --
-------------------------------------------------------------------------------
semigroupTests :: IO ()
semigroupTests = do
    quickCheck (semigroupAssoc :: TrivialAssoc)
    quickCheck (semigroupAssoc :: IdentityAssoc)
    quickCheck (semigroupAssoc :: TwoAssoc)
    quickCheck (semigroupAssoc :: ThreeAssoc)
    quickCheck (semigroupAssoc :: FourAssoc)
    quickCheck (semigroupAssoc :: BoolConjAssoc)
    quickCheck (semigroupAssoc :: BoolDisjAssoc)
    quickCheck (semigroupAssoc :: OrAssoc)
    -- quickCheck (semigroupAssoc :: CombAssoc)
    quickCheck (semigroupAssoc :: ValidationAssoc)
    quickCheck (semigroupAssoc :: AccumulateRightAssoc)
    quickCheck (semigroupAssoc :: AccumulateBothAssoc)


semigroupFunctionTests :: IO ()
semigroupFunctionTests = do
  putStrLn "\nTesting Semigroup Combine and Comp with hspec"
  hspec $ do
    describe "Semigroup instance for Combine (Sum Int)" $ do
     -- the examples from the book
      it "comb1 <> comb2 $ 0 should be 0" $ do
        (unCombine (comb1 <> comb2) $ 0) == (Sum 0)
      it "comb1 <> comb2 $ 1 should be 2" $ do
        (unCombine (comb1 <> comb2) $ 1) == (Sum 2)
      it "comb1 <> comb1 $ 0 should be 2" $ do
        (unCombine (comb1 <> comb1) $ 0) == (Sum 2)
      it "comb2 <> comb2 $ 1 should be 0" $ do
        (unCombine (comb2 <> comb2) $ 1) == (Sum 0)
      it "Combine is associative for a specific range" $ do
        all (\x -> comb12with3 x == comb1with23 x) intRange
    describe "Semigroup instance for Comp (Sum Int)" $ do
      it "Comp comp1 and comp2 with 0 should be 0" $ do
        (unComp (comp1 <> comp2) $ 0) == (Sum 1)
      it "Combine comp1 and comp2 with 1 should be 2" $ do
        (unComp (comp1 <> comp2) $ 2) == (Sum 5)
      it "Comp comp1 with iotself to get + 2" $ do
        (unComp (comp1 <> comp1) $ 0) == (Sum 2)
      it "Combine comp2 with itself to get * 4" $ do
        (unComp (comp2 <> comp2) $ 2) == (Sum 8)
      it "Comp is associative for a specific range" $ do
        all (\x -> comb12with3 x == comb1with23 x) intRange

-------------------------------------------------------------------------------
--                              Monoid exercises                             --
-------------------------------------------------------------------------------
-- Exercise 1.
instance Monoid Trivial where
    mempty = Trivial

-- Exercise 2.
instance Monoid a => Monoid (Identity a) where
    mempty = Identity mempty

-- Exercise 3.
instance (Monoid a, Monoid b) => Monoid (Two a b) where
    mempty = Two mempty mempty

-- Exercise 4.
instance Monoid BoolConj where
    mempty = BoolConj True

-- Exercise 5.
instance Monoid BoolDisj where
    mempty = BoolDisj False

-- Exercise 6.
instance Monoid b => Monoid (Combine a b) where
    mempty = Combine (id mempty)

-- Exercise 7.
instance Monoid a => Monoid (Comp a) where
    mempty = Comp id

-- Exercise 8.
newtype Mem s a = Mem { runMem :: s -> (a, s) }
type MemTest = Mem Int String

instance Semigroup a => Semigroup (Mem s a) where
    (Mem f) <> (Mem g) = Mem go where
        go s = let (fa, fs) = f s
                   (ga, gs) = g fs in
               (fa <> ga, gs)

instance Monoid a => Monoid (Mem s a) where
    mempty = Mem (\s -> (mempty, s))


-------------------------------------------------------------------------------
--                           Test Monoid exercises                           --
-------------------------------------------------------------------------------
monoidTests :: IO ()
monoidTests = do
  quickCheck (monoidLeftIdentity ::  Trivial -> Bool)
  quickCheck (monoidRightIdentity :: Trivial -> Bool)
  quickCheck (monoidLeftIdentity ::  IdTest -> Bool)
  quickCheck (monoidRightIdentity :: IdTest -> Bool)
  quickCheck (monoidLeftIdentity ::  TwoTest -> Bool)
  quickCheck (monoidRightIdentity :: TwoTest -> Bool)
  quickCheck (monoidLeftIdentity ::  BoolConj -> Bool)
  quickCheck (monoidRightIdentity :: BoolConj -> Bool)
  quickCheck (monoidLeftIdentity ::  BoolDisj -> Bool)
  quickCheck (monoidRightIdentity :: BoolDisj -> Bool)
  -- putStrLn "\nTesting Monoid Combine, Comp and Mem with hspec"
  testMonoidCombine
  testMonoidComp
  testMonoidMem

testMonoidCombine :: IO ()
testMonoidCombine = hspec $ do
  describe "Monoid example for Combine (Sum Int) from book" $ do
     it "unCombine (mappend comb1 mempty) $ 1 == (Sum 2)" $ do
       (unCombine (mappend comb1 mempty) $ 1) == (Sum 2)
  describe "Monoid Laws for Combine (Sum Int)" $ do
     it "Left identity" $ do
       all (\x -> (unCombine (mappend mempty comb1)) x == (unCombine comb1) x) intRange
  describe "Right Identity" $ do
     it "Right Identity" $ do
       all (\x -> (unCombine (mappend comb1 mempty)) x == (unCombine comb1) x) intRange

testMonoidComp :: IO ()
testMonoidComp = hspec $ do
  describe "Monoid example for Comp (Sum Int)" $ do
     it "unComp (mappend comp1 mempty) $ 1 should be (Sum 2)" $ do
       (unComp (mappend comp1 mempty) $ 1) == (Sum 2)
  describe "Monoid Laws for Comp (Sum Int)" $ do
     it "Left identity" $ do
       all (\x -> (unComp (mappend mempty comp1)) x == (unComp comp1) x) $ map Sum intRange
     it "Right Identity" $ do
       all (\x -> (unComp (mappend comp1 mempty)) x == (unComp comp1) x) $ map Sum intRange

testMonoidMem :: IO ()
testMonoidMem = hspec $ do
  describe "Semigroup Laws form Mem Int String" $ do
    it "Associativity over a specific range" $ do
      all (\a -> runMem ((mem1 <> mem2) <> mem3) a == runMem (mem1 <> (mem2 <> mem3)) a) intRange
  describe "Monoid examples from the book" $ do
    it "Left identity example from book" $ do
      runMem (mem1 <> mempty) 0 == ("hello", 1)
    it "f <> mempty == f" $ do
      runMem (mempty <> mem1) 0 == runMem mem1 0
    it "mempty <> f == f" $ do
      runMem (mempty <> mem1) 0 == runMem mem1 0
    it "Right identity example from book" $ do
      runMem (mempty <> mem1) 0 == ("hello", 1)
    it "runMem mempty 0 = (\"\", 0)" $ do
      runMem mempty 0 == ("", 0)
  describe "Monoid Laws" $ do
     it "Left identity" $ do
       all (\x -> (runMem (mappend mempty mem1)) x == (runMem mem1) x) intRange
     it "Right identity" $ do
       all (\x -> (runMem (mappend mem1 mempty)) x == (runMem mem1) x) intRange

-------------------------------------------------------------------------------
--                 Support for testing Combine, Comp and Mem                 --
-------------------------------------------------------------------------------
intRange :: [Int]
intRange = [-200,-197..200]

comb1 :: Combine Int (Sum Int)
comb1 = Combine (\n -> Sum (n + 1))
comb2 :: Combine Int (Sum Int)
comb2 = Combine (\n -> Sum (n - 1))
comb3 :: Combine Int (Sum Int)
comb3 = Combine (\n -> Sum (5 * n))

comb12with3 :: Int -> Sum Int
comb12with3 = unCombine ((comb1 <> comb2) <> comb3)
comb1with23 :: Int -> Sum Int
comb1with23 = unCombine (comb1 <> (comb2 <> comb3))

comp1 :: Comp (Sum Int)
comp1 = Comp (\(Sum n) -> Sum (n + 1))
comp2 :: Comp (Sum Int)
comp2 = Comp (\(Sum n) -> Sum (2 * n))
comp3 :: Comp (Sum Int)
comp3 = Comp (\(Sum n) -> Sum (5 * n))

comp12with3 :: Sum Int -> Sum Int
comp12with3 = unComp ((comp1 <> comp2) <> comp3)
comp1with23 :: Sum Int -> Sum Int
comp1with23 = unComp (comp1 <> (comp2 <> comp3))

mem1 :: MemTest
mem1 = Mem $ \s -> ("hello", s + 1)
mem2 :: MemTest
mem2 = Mem $ \s -> (", ", s * 3)
mem3 :: MemTest
mem3 = Mem $ \s -> ("world", 21)


-------------------------------------------------------------------------------
--                               Run all tests                               --
-------------------------------------------------------------------------------
underline :: String
underline = take 48 $ repeat '='

main :: IO ()
main = do
  putStrLn $ "\n\nSemigroup tests\n" ++ underline
  semigroupTests
  putStrLn $ "\n\nSemigroup Function tests\n" ++ underline
  semigroupFunctionTests
  putStrLn $ "\n\nMonoid tests\n" ++ underline
  monoidTests

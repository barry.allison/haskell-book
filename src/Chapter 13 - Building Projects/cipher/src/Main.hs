module Main where

import           Control.Monad (forever)
import           Data.Char
import           System.Exit   (exitSuccess)

shiftRight :: Int -> Char -> Char
shiftRight n c
    | isUpper c = wrapChar (ord 'A') n c
    | isLower c = wrapChar (ord 'a') n c
    | otherwise = c

wrapChar :: Int -> Int -> Char -> Char
wrapChar start offSet c = chr (start + (ord c + offSet - start) `mod` 26)

charShift :: Char -> Int
charShift c = ord c - ord 'A'

caesar :: String -> Int ->String
caesar s n = map (shiftRight n) s

unCaesar :: String -> Int ->String
unCaesar s n = caesar s (26 - n)

vigenere :: String -> String -> String
vigenere plain key = go plain (cycle key)
    where go [] _          = []
          go (' ':ps) key  = ' ' : go ps key
          go (p:ps) (k:ks) = shiftRight (charShift k) p : go ps ks

unVigenere :: String -> String -> String
unVigenere plain key = go plain (cycle key)
    where go [] _          = []
          go (' ':ps) key  = ' ' : go ps key
          go (p:ps) (k:ks) = shiftRight (26 - (charShift k)) p : go ps ks


encodeDecode :: String -> IO ()
encodeDecode menuChoice = do
  key <- getResponse "Enter key: "
  text <- getResponse "Enter text: "
  case menuChoice of
    "1" -> putStrLn $ caesar text (read key :: Int)
    "2" -> putStrLn $ vigenere text key
    "3" -> putStrLn $ unCaesar text (read key :: Int)
    "4" -> putStrLn $ unVigenere text key

getResponse :: String -> IO String
getResponse prompt = do
  putStr prompt
  response <- getLine
  return response

menu :: String
menu = "\n" ++
       "1. Encode with Caesar cipher.\n" ++
       "2. Decode with Caesar cipher.\n" ++
       "3. Encode with Vigenercipher.\n" ++
       "4. Decode with Vigener cipher.\n" ++
       "9. Quit.\n"

main :: IO ()
main = forever $ do
  menuChoice <- getResponse menu
  case menuChoice of
    "1" -> encodeDecode menuChoice
    "2" -> encodeDecode menuChoice
    "3" -> encodeDecode menuChoice
    "4" -> encodeDecode menuChoice
    "9" -> exitSuccess  -- quit
    _   -> putStrLn "Invalid choice, try again\n"

See Main.hs in hangman/src/

Hangman game logic
----------------------------------------------------------------

The main changes I made were:
 1. Limit words to alphabetic characters only -
    my dictionary has a lot of words with apostrohes).
 2. Regular hangman (when I was a kid) gave you 13 incorrect guesses, not 7.
 3. Evaluate gameWin before gameOver to avoid losing
    when you guess correctly on your last attempt.

Change 1.
allWords :: IO WordList
allWords = do
  dict <- readFile "data/dict.txt"
  return $ WordList (filter alphaOnly . lines $ dict)

alphaOnly :: String -> Bool
alphaOnly = all (`elem` ['a'..'z'])

Change 2.
gameOver :: Puzzle -> IO ()
gameOver (Puzzle wordToGuess _ guessed) =
    if (length guessed) > 12
    then do putStrLn "You lose!"
            putStrLn $ "The word was: " ++ wordToGuess
            exitSuccess
    else return ()

Change 3.
runGame :: Puzzle -> IO ()
runGame puzzle = forever $ do
    gameWin puzzle
    gameOver puzzle
    putStrLn $ "Current puzzle is: " ++ show puzzle
    putStr "Guess a letter: "
    guess <- getLine
    case guess of
      [c] -> handleGuess puzzle c >>= runGame
      _ -> putStrLn "Your guess must\
                    \ be a single character"

Modifying code
----------------------------------------------------------------

1. See xsMain.hs in cipher/src/

encodeDecode :: String -> IO ()
encodeDecode menuChoice = do
  key <- getResponse "Enter key: "
  text <- getResponse "Enter text: "
  case menuChoice of
    "1" -> putStrLn $ caesar text (read key :: Int)
    "2" -> putStrLn $ vigenere text key
    "3" -> putStrLn $ unCaesar text (read key :: Int)
    "4" -> putStrLn $ unVigenere text key

getResponse :: String -> IO String
getResponse prompt = do
  putStr prompt
  response <- getLine
  return response

menu :: String
menu = "\n" ++
       "1. Encode with Caesar cipher.\n" ++
       "2. Decode with Caesar cipher.\n" ++
       "3. Encode with Vigenercipher.\n" ++
       "4. Decode with Vigener cipher.\n" ++
       "9. Quit.\n"

main :: IO ()
main = forever $ do
  menuChoice <- getResponse menu
  case menuChoice of
    "1" -> encodeDecode menuChoice
    "2" -> encodeDecode menuChoice
    "3" -> encodeDecode menuChoice
    "4" -> encodeDecode menuChoice
    "9" -> exitSuccess  -- quit
    _   -> putStrLn "Invalid choice, try again\n"

2. See PalindromeIO.hs

palindrome :: IO ()
palindrome = forever $ do
    line1 <- getLine
    case (line1 == reverse line1) of
      True  -> putStrLn "It's a palindrome!"
      False -> putStrLn "Nope!"


import           Control.Monad (forever)
import           Data.Char     (toLower)
import           System.Exit   (exitSuccess)

3. Palindrome
palindrome' :: IO ()
palindrome' = forever $ do
    line1 <- getLine
    case (line1 == reverse line1) of
      True  -> do putStrLn "It's a palindrome!"
                  exitSuccess
      False -> putStrLn "Nope!"

lowerAlpha :: String -> String
lowerAlpha = filter (`elem` ['a'..'z']) . map toLower

palindrome'' :: IO ()
palindrome'' = forever $ do
    line1 <- getLine
    let normalised =  lowerAlpha line1
    case (normalised == reverse normalised) of
      True  -> do putStrLn "It's a palindrome!"
                  exitSuccess
      False -> putStrLn "Nope!"

4. Make Person
gimmePerson :: IO ()
gimmePerson = do
  putStr "Name: "
  name <- getLine
  putStr "Age: "
  age <- getLine
  case mkPerson name (read age :: Integer) of
    Right p -> putStrLn $ "Yay! Successfully got a person: " ++ show p
    Left pi -> putStrLn $ "Invalid: " ++ show pi

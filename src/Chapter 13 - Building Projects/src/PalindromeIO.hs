module PalindromeIO where

import           Control.Monad (forever)
import           Data.Char     (isAlpha, toLower)
import           System.Exit   (exitSuccess)

palindrome :: IO ()
palindrome = forever $ do
    line1 <- getLine
    case (line1 == reverse line1) of
      True  -> do putStrLn "It's a palindrome!"
                  exitSuccess
      False -> putStrLn "Nope!"

lowerAlpha :: String -> String
lowerAlpha = filter isAlpha . map toLower

palindrome' :: IO ()
palindrome' = forever $ do
    line1 <- getLine
    let normalised =  lowerAlpha line1
    case (normalised == reverse normalised) of
      True  -> do putStrLn "It's a palindrome!"
                  exitSuccess
      False -> putStrLn "Nope!"

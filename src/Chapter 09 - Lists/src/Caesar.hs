module Caesar where

import           Data.Char

caesar :: String -> Int ->String
caesar s n = map (shiftRight n) s

unCaesar :: String -> Int ->String
unCaesar s n = caesar s (26 - n)

shiftRight :: Int -> Char -> Char
shiftRight n c
    | isUpper c = wrapChar (ord 'A') n c
    | isLower c = wrapChar (ord 'a') n c
    | otherwise = c

wrapChar :: Int -> Int -> Char -> Char
wrapChar start offSet c = chr (start + (ord c + offSet - start) `mod` 26)

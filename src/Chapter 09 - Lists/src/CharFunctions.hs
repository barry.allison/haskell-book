module CharFunctions where

import           Data.Char

allUpper :: String -> String
allUpper = filter isUpper

capitalise :: String -> String
capitalise []     = []
capitalise (x:xs) = toUpper x : xs

capitalise' :: String -> String
capitalise' = map toUpper

capitalise'' :: String -> String
capitalise'' []     = []
capitalise'' (x:xs) = toUpper x : capitalise'' xs

upperInitial :: String -> Char
upperInitial s = toUpper $ head s

upperInitial' :: String -> Char
upperInitial' s = toUpper . head $ s

upperInitial'' :: String -> Char
upperInitial'' = toUpper . head

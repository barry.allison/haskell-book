module StandardFunctions where

myOr :: [Bool] -> Bool
myOr []     = False
myOr (x:xs) = x || myOr xs

myAny :: (a -> Bool) -> [a] -> Bool
myAny f []     = False
myAny f (x:xs) = f x || myAny f xs

myElem :: Eq a => a -> [a] -> Bool
myElem _ []     = False
myElem x (y:ys) = x == y || myElem x ys

myElem' :: Eq a => a -> [a] -> Bool
myElem' x xs = any (== x) xs

myReverse :: [a] -> [a]
myReverse xs = go xs []
    where
      go [] ys     = ys
      go (x:xs) ys = go xs (x:ys)

squish :: [[a]] -> [a]
squish []     = []
squish (x:xs) = x ++ squish xs

squishMap :: (a -> [b]) -> [a] -> [b]
squishMap _ []     = []
squishMap f (x:xs) = f x ++ squishMap f xs

squishAgain :: [[a]] -> [a]
squishAgain = squishMap id

myMaximumBy :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy _ [] = error "myMaximumBy - empty list"
myMaximumBy _ [x] = x
myMaximumBy f (x1:x2:xs) =
    case f x1 x2 of
      GT -> myMaximumBy f (x1:xs)
      _  -> myMaximumBy f (x2:xs)

myMinimumBy :: (a -> a -> Ordering) -> [a] -> a
myMinimumBy _ [] = error "myMaximumBy - empty list"
myMinimumBy _ [x] = x
myMinimumBy f (x1:x2:xs) =
    case f x1 x2 of
      LT -> myMinimumBy f (x1:xs)
      _  -> myMinimumBy f (x2:xs)

myMaximum :: (Ord a) => [a] -> a
myMaximum = myMaximumBy compare

myMinimum :: (Ord a) => [a] -> a
myMinimum = myMinimumBy compare

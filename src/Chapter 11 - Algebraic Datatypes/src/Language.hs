module Language where

import           Data.Char
import           Data.List

capitalizeParagraph :: String -> String
capitalizeParagraph =
    unwords . addPeriod . joinSentence . sentenceCase . sentences

sentences :: String -> [[String]]
sentences = map words . splitOn '.'

sentenceCase :: [[String]] -> [[String]]
sentenceCase = map capitalizeSentence

joinSentence :: [[String]] -> [String]
joinSentence = map unwords

addPeriod :: [String] -> [String]
addPeriod = map (append ".")

append :: [a] -> [a] -> [a]
append = flip (++)

capitalizeSentence :: [String] -> [String]
capitalizeSentence []     = []
capitalizeSentence (w:ws) = capitalizeWord w : ws

capitalizeWord :: String -> String
capitalizeWord []     = []
capitalizeWord (c:cs) = toUpper c : cs

-- re-use splitOn from Cha[pter 9
splitOn :: Char -> String -> [String]
splitOn x [] = []
splitOn x ws@(c:cs)
        | c == x   = splitOn x cs
        | otherwise = firstSplit : splitOn x unSplit
        where (firstSplit, unSplit) = break (== x) ws

isPeriod :: Char -> Bool
isPeriod = (== '.')

{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Goats where

class TooMany a where
    tooMany :: a -> Bool

instance TooMany Int where
    tooMany n = n > 42

-- Different instance behaviour for newtype
newtype Goats = Goats Int deriving (Eq, Show)
instance TooMany Goats where
    tooMany (Goats n) = n > 43

-- Manual typeclass implementation
newtype Cows = Cows Int deriving (Eq, Show)
instance TooMany Cows where
    tooMany (Cows n) = tooMany n

-- Automatically deriving typeclass (with GeneralizedNewtypeDeriving pragma)
newtype Pigs = Pigs Int deriving (Eq, Show, TooMany)

instance TooMany (Int, String) where
    tooMany (n, _) = tooMany n
newtype Dogs = Dogs (Int, String) deriving (Eq, Show, TooMany)

instance TooMany (Int, Int) where
    tooMany (x, y) = tooMany (x + y)
newtype TvDogs = TvDogs (Int, String) deriving (Eq, Show, TooMany)

instance (Num a, TooMany a) => TooMany (a, a) where
    tooMany (x, y) = tooMany (x + y)

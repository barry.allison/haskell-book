module BinaryTree where

data BinaryTree a =
    Leaf
  | Node (BinaryTree a) a (BinaryTree a)
    deriving (Eq, Ord, Show)

insert' :: Ord a => a -> BinaryTree a -> BinaryTree a
insert' x Leaf = Node Leaf x Leaf
insert' x (Node left y right)
    | x == y = Node left x right
    | x < y = Node (insert' x left) y right
    | x > y = Node left y (insert' x right)

mapTree :: (a -> b) -> BinaryTree a -> BinaryTree b
mapTree _ Leaf                = Leaf
mapTree f (Node left x right) = Node (mapTree f left) (f x) (mapTree f right)

testTree' :: BinaryTree Integer
testTree' = Node (Node Leaf 3 Leaf) 1 (Node Leaf 4 Leaf)

mapExpected :: BinaryTree Integer
mapExpected = Node (Node Leaf 4 Leaf) 2 (Node Leaf 5 Leaf)

-- acceptance test for mapTree
mapOkay = if mapTree (+1) testTree' == mapExpected
          then print "yup okay!"
          else error "test failed!"

preorder :: BinaryTree a -> [a]
preorder Leaf                = []
preorder (Node left x right) = [x] ++ preorder left ++ preorder right

inorder :: BinaryTree a -> [a]
inorder Leaf                = []
inorder (Node left x right) = inorder left ++ [x] ++ inorder right

postorder :: BinaryTree a -> [a]
postorder Leaf                = []
postorder (Node left x right) = postorder left ++ postorder right ++ [x]

testTree :: BinaryTree Integer
testTree = Node (Node Leaf 1 Leaf) 2 (Node Leaf 3 Leaf)

testPreorder :: IO ()
testPreorder = if preorder testTree == [2, 1, 3]
               then putStrLn "Preorder fine!"
               else putStrLn "Bad news bears."

testInorder :: IO ()
testInorder = if inorder testTree == [1, 2, 3]
              then putStrLn "Inorder fine!"
              else putStrLn "Bad news bears."

testPostorder :: IO ()
testPostorder = if postorder testTree == [1, 3, 2]
                then putStrLn "Postorder fine!"
                else putStrLn "postorder failed check"

foldTree :: (a -> b -> b) -> b -> BinaryTree a -> b
foldTree = inOrderFold

-- preorder so f x result is used as argument to
-- preOrderFold f _ left and that result is used in
-- preOrderFold f _ right
preOrderFold :: (a -> b -> b) -> b -> BinaryTree a -> b
preOrderFold _ result Leaf = result
preOrderFold f result (Node left x right) =
    preOrderFold f (preOrderFold f (f x result) left) right

-- inorder so inOrderFold f result left is  an argument to
-- f x _ and that result is used in
-- inOrderFold f _ right
inOrderFold :: (a -> b -> b) -> b -> BinaryTree a -> b
inOrderFold _ result Leaf = result
inOrderFold f result (Node left x right) =
    inOrderFold f (f x (inOrderFold f result left)) right

-- post order so
-- postOrderFold f result left is used in
-- postOrderFold _ right and that result is used in
-- f x _
postOrderFold :: (a -> b -> b) -> b -> BinaryTree a -> b
postOrderFold _ result Leaf = result
postOrderFold f result (Node left x right) =
      f x (postOrderFold f (postOrderFold f result left) right)

-- https://en.wikipedia.org/wiki/Tree_traversal
wikiTree :: BinaryTree String
wikiTree =
    Node
      (Node -- left sub-tree
         (Node Leaf "A" Leaf)
         "B"
         (Node
            (Node Leaf "C" Leaf)
            "D"
            (Node Leaf "E" Leaf)))
      "F"  -- root
      (Node
       Leaf
       "G"  -- right sub-tree
       (Node
        (Node Leaf
              "H"
              Leaf)
        "I"
        Leaf))

append :: [a] -> [a] -> [a]
append = flip (++)

testPreOrderFold :: IO ()
testPreOrderFold =
    if visitNodes wikiTree == "FBADCEGIH"
    then putStrLn "Preorder fold fine!"
    else putStrLn "Preorder failed check"
    where visitNodes = preOrderFold append ""

testInOrderFold :: IO ()
testInOrderFold =
    if visitNodes wikiTree == "ABCDEFGHI"
    then putStrLn "Inorder fold fine!"
    else putStrLn "Inorder failed check"
    where visitNodes = inOrderFold append ""


testPostOrderFold :: IO ()
testPostOrderFold =
    if visitNodes wikiTree == "ACEDBHIGF"
    then putStrLn "Postorder fold fine!"
    else putStrLn "Postorder failed check"
    where visitNodes = postOrderFold append ""

main :: IO ()
main = do testPreorder
          testInorder
          testPostorder
          testPreOrderFold
          testInOrderFold
          testPostOrderFold

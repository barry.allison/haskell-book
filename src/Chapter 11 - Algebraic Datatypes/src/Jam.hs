module Jam where

import           Data.List (groupBy, maximumBy, sortBy)

data Fruit =
    Peach
  | Plum
  | Apple
  | Blackberry
    deriving (Eq, Show, Ord)

data JamJars =
    Jam Fruit Int
    deriving (Eq, Show, Ord)

data JamJars' =
    Jam' { fruit :: Fruit
         , count :: Int }
    deriving (Eq, Show, Ord)

row1 = Jam' Peach 1
row2 = Jam' Plum 12
row3 = Jam' {fruit = Apple, count = 1}
row4 = Jam' {fruit = Blackberry, count = 36}
row5 = Jam' {fruit = Blackberry, count = 48}
row6 = Jam' {fruit = Plum, count = 15}
allJam = [row1, row2, row3, row4, row5, row6]

jarCounts :: [JamJars'] -> [Int]
jarCounts = map count

howManyJars :: [JamJars'] -> Int
howManyJars = sum . jarCounts  -- sum . map count

mostRow :: [JamJars'] -> JamJars'
mostRow = maximumBy compare

compareKind :: JamJars' -> JamJars' -> Ordering
compareKind (Jam' k _) (Jam' k' _) = compare k k'

sortedJars = sortBy compareKind allJam

letsAllJamTogether = groupBy (==) sortedJars

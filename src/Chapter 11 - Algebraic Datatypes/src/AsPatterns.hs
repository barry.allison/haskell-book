module AsPatterns where

import           Data.Char

isSubsequenceOf :: (Eq a) => [a] -> [a] -> Bool
isSubsequenceOf [] _ = True
isSubsequenceOf _ [] = False
isSubsequenceOf xs@(x:xt) (y:yt)
    | x == y    = isSubsequenceOf xt yt
    | otherwise = isSubsequenceOf xs yt

capitalizeWords :: String -> [(String, String)]
capitalizeWords = map tuplise . words
    where tuplise cs@(c:ct) = (cs, toUpper c : ct)

module Phone where

import           Data.Char (isSpace, isUpper, toLower)
import           Data.List (elemIndex, groupBy, maximumBy, sort)
import           Data.Ord  (comparing)

data Key = Key {digit   :: Digit,
                letters :: String }
           deriving (Eq, Ord, Show)

data DaPhone = DaPhone [Key]
               deriving (Eq, Ord, Show)

convo :: [String]
convo = ["Wanna play 20 questions",
         "Ya",
         "U 1st haha",
         "Lol ok. Have u ever tasted alcohol lol",
         "Lol ya",
         "Wow ur cool haha. Ur turn",
         "Ok. Do u think I am pretty Lol",
         "Lol ya",
         "Haha thanks just making sure rofl ur turn"]

-- validButtons = "1234567890*#"
type Digit = Char

-- Valid presses: 1 and up
type Presses = Int

reverseTaps :: DaPhone -> Char -> [(Digit, Presses)]
reverseTaps p c | isUpper c = ('*', 1) : reverseTaps p (toLower c)
                | otherwise = [taps p c]

taps :: DaPhone -> Char -> (Digit, Presses)
taps p c = case elemIndex c cs of
             Just i  -> (key,  i + 1)
             Nothing -> error $ "taps - unknown character" ++ [c]
    where (Key key cs)  = charKey p c

charKey :: DaPhone -> Char -> Key
charKey (DaPhone keys) c = head digits
    where digits = filter (elem c . letters) keys


cellPhonesDead :: DaPhone -> String -> [(Digit, Presses)]
cellPhonesDead = concatMap . reverseTaps

phone = DaPhone [Key '1' "1",     Key '2' "abc2", Key '3' "def3",
                 Key '4' "ghi4",  Key '5' "jkl5", Key '6' "mno6",
                 Key '7' "pqrs7", Key '8' "tuv8", Key '9' "wxyz9",
                 Key '*' "^",     Key '0' " 0",   Key '#' ".,"]


fingerTaps :: [(Digit, Presses)] -> Presses
fingerTaps = sum . map snd

-- map fingerTaps . map (cellPhonesDead phone) $ convo
-- [48,5,17,81,15,49,58,15,80]

-- generic helper
mostPopular :: Ord a => [a] -> a
mostPopular = head .
              maximumBy (comparing length) .  -- longest
              groupBy (==) .   -- same items together
              sort             -- order

mostPopularLetter :: String -> Char
mostPopularLetter = mostPopular

coolestLtr :: [String] -> Char
coolestLtr = mostPopularLetter . concat

coolestLtr' :: [String] -> Char
coolestLtr' = mostPopularLetter . filter (not . isSpace) . concat

coolestWord :: [String] -> String
coolestWord  = mostPopular . concatMap words

module Hutton where

data Expr
    = Lit Integer
    | Add Expr Expr
    deriving (Eq,  Show)

eval :: Expr -> Integer
eval (Lit x)   = x
eval (Add a b) = eval a + eval b

printExpr :: Expr -> String
printExpr (Lit x)   = show x
printExpr (Add a b) = printExpr a ++ " + " ++ printExpr b

t = (Add (Lit 1) (Lit 9001))
a1 = Add (Lit 9001) (Lit 1)
a2 = Add a1 (Lit 20001)
a3 = Add (Lit 1) a2

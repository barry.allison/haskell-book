module Vigenere where

import           Caesar    (shiftRight)
import           Data.Char

charShift :: Char -> Int
charShift c = ord c - ord 'A'

vigenere :: String -> String -> String
vigenere plain key = go plain (cycle key)
    where go [] _          = []
          go (' ':ps) key  = ' ' : go ps key
          go (p:ps) (k:ks) = shiftRight (charShift k) p : go ps ks

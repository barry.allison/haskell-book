module MatchTheTypes where
import           Data.List (sort)

-- 1.
----------------------------------------------------------------
i :: Num a => a
i = 1
-- i :: a

{-
Doesn't typecheck
a is too general  e.g. concrete type of a could be Char in i :: a
-}

-- 2.
----------------------------------------------------------------
f2 :: Float
f2 = 1.0
-- f2 :: Num a => a

{-
Doesn't typecheck
:t 1.0
Fractional a => a,
Num a => a is too general e.g. concrete type for a includes Int and Integer
-}

-- 3.
----------------------------------------------------------------
f3 :: Float
f3 = 1.0
-- f3 :: Fractional a => a

{-
Typechecks.
:t 1.0
Fractional a => a
-}

-- 4. Hint for the following: type :info RealFrac in your REPL.
----------------------------------------------------------------
f4 :: Float
f4 = 1.0
-- f4 :: RealFrac a => a

{-
Typechecks.
: info RealFrac
class (Real a, Fractional a) => RealFrac a where ...
:t 1.0
Fractional a => a
-}

-- 5.
----------------------------------------------------------------
freud :: a -> a
freud x = x

-- freud :: Ord a => a -> a

{-
Typechecks.
freud :: a -> a typechecks
the more concrete
freud Ord a => a -> a must also typecheck
-}

-- 6.
----------------------------------------------------------------
freud' :: a -> a
freud' x = x
--freud' :: Int -> Int

{-
Typechecks.
freud' :: a -> a typechecks
the more concrete
freud' Int -> Int must also typecheck
-}

-- 7.
----------------------------------------------------------------
myX = 1 :: Int
sigmund :: Int -> Int
sigmund x = myX
--sigmund :: a -> a

{-
Doesn't typecheck
sigmund 'a' is type
sigmund :: Char -> Char but
myX :: Int
-}

--8.
----------------------------------------------------------------

myX' = 1 :: Int
sigmund' :: Int -> Int
sigmund' x = myX
-- sigmund' :: Num a => a -> a

{-
Doesn't typecheck
sigmund 1.0 is type
sigmund :: Fractional a => a -> a
myX :: Int
-}

-- 9. You’ll need to import sort from Data.List.
----------------------------------------------------------------
-- import Data.List (sort)
jung :: Ord a => [a] -> a
jung xs = head (sort xs)
-- jung :: [Int] -> Int

{-
Typechecks.
:t sort
sort :: Ord a => [a] -> [a]
jung :: [Int] -> Int is OK - Int has an instance of Ord
-}

-- 10 .
----------------------------------------------------------------
young :: [Char] -> Char
young xs = head (sort xs)
-- young :: Ord a => [a] -> a

{-
Typechecks.
:t sort
sort :: Ord a => [a] -> [a]
so :t head becomes
head :: Ord a => [a] -> a
which is more concrete than [a] -> a
-}

-- 11.
----------------------------------------------------------------
mySort :: [Char] -> [Char]
mySort = sort
signifier :: [Char] -> Char
signifier xs = head (mySort xs)
-- signifier :: Ord a => [a] -> a

{-
Doesn't Typecheck.
signifier [1.0, 2.0] is valid with type
signifier :: Fractional a => [a] -> a
-}

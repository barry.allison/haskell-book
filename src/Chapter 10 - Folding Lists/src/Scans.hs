module Scans where

fibs :: [Integer]
fibs = 1 : scanl (+) 1 fibs
fibsN x = fibs !! x

fibs' :: [Integer]
fibs' = take 20 fibs

fibs'' :: [Integer]
fibs'' = takeWhile (< 100) fibs

factorials :: [Integer]
factorials = scanl (*) 1 [1..]

factorial :: Int -> Integer
factorial x = factorials !! x

seekritFunc :: String -> Int
seekritFunc x =
    div (sum (map length (words x)))
            (length (words x))

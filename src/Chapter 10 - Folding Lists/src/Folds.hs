module Folds where

-- recursive
myOr :: [Bool] -> Bool
myOr []     = False
myOr (x:xs) = x || myOr xs
-- fold / point-free
myOr' :: [Bool] -> Bool
myOr' = foldr (||) False

-- recursive
myAny :: (a -> Bool) -> [a] -> Bool
myAny f = foldr (\x res -> res || f x) False
-- fold / point-free
myAny' :: (a -> Bool) -> [a] -> Bool
myAny' f = myOr . map f

-- recursive
myElem :: Eq a => a -> [a] -> Bool
myElem _ []     = False
myElem a (b:bs) = a == b || myElem a bs
-- fold / point-free
myElem' :: Eq a => a -> [a] -> Bool
myElem' a = foldr (\b res -> a == b || res) False
-- fold / point-free
myElem'' :: Eq a => a -> [a] -> Bool
myElem'' a = foldr ((||) . (a ==)) False

-- recursive
myReverse :: [a] -> [a]
myReverse xs = go xs []
    where go [] ys     = ys
          go (x:xs) ys = go xs (x:ys)
-- fold / point-free
myReverse' :: [a] -> [a]
myReverse' = foldl (flip (:)) []

-- recursive
myMap :: (a -> b) -> [a] -> [b]
myMap f []     = []
myMap f (x:xs) = f x : myMap f xs
-- fold / point-free
myMap' :: (a -> b) -> [a] -> [b]
myMap' f = foldr ((:) . f) []


-- recursive
myFilter :: (a -> Bool) -> [a] -> [a]
myFilter f [] = []
myFilter f (x:xs) = if f x
                    then x : myFilter f xs
                    else myFilter f xs
-- fold / point-free
myFilter' :: (a -> Bool) -> [a] -> [a]
myFilter' f = foldr consIf  []
    where  x `consIf` res | f x = x : res
                          | otherwise = res

-- recursive
squish :: [[a]] -> [a]
squish []       = []
squish (xs:xss) = xs ++ squish xss
-- fold / point-free
squish' :: [[a]] -> [a]
squish' = foldr (++) []

-- recursive
squishMap :: (a -> [b]) -> [a] -> [b]
squishMap f []       = []
squishMap f (xs:xss) = f xs ++ squishMap f xss

-- fold / point-free
squishMap' :: (a -> [b]) -> [a] -> [b]
squishMap' f = foldr ((++) . f) []

squishMap'' :: (a -> [b]) -> [a] -> [b]
squishMap'' f = squish . map f

squishAgain :: [[a]] -> [a]
squishAgain = squishMap id

-- recursive (from Chapter 9)
myMaximumBy :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy _ [] = error "myMaximumBy - empty list"
myMaximumBy _ [x] = x
myMaximumBy f (x1:x2:xs) =
    case f x1 x2 of
      GT -> myMaximumBy f (x1:xs)
      _  -> myMaximumBy f (x2:xs)

-- fold / point-free
myMaximumBy' :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy' f xs = foldr1 comparing xs
    where comparing x y = case f x y of
                            GT -> x
                            _  -> y

-- fold / point-free
myMinimumBy :: (a -> a -> Ordering) -> [a] -> a
myMinimumBy f xs = foldr1 comparing xs
    where comparing x y = case f x y of
                            LT -> x
                            _  -> y

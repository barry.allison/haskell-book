module Database where

import           Data.Time

data DatabaseItem = DbString String
                  | DbNumber Integer
                  | DbDate UTCTime
                  deriving (Eq, Ord, Show)

theDatabase :: [DatabaseItem]
theDatabase =
  [ DbDate (UTCTime
            (fromGregorian 1911 5 1)
            (secondsToDiffTime 34123))
  , DbNumber 9001
  , DbNumber 8999
  , DbString "Hello, world!"
  , DbDate (UTCTime
              (fromGregorian 1921 5 1)
              (secondsToDiffTime 34123))
  ]

filterDbDate :: [DatabaseItem] -> [UTCTime]
filterDbDate = foldr f []
    where f (DbDate t) ts = t : ts
          f _          ts = ts

filterDbNumber :: [DatabaseItem] -> [Integer]
filterDbNumber = foldr f []
    where f (DbNumber n) ns = n : ns
          f _            ns = ns

mostRecent :: [DatabaseItem] -> UTCTime
mostRecent = maximum . filterDbDate

mostRecent' :: [DatabaseItem] -> UTCTime
mostRecent' = foldr1 max . filterDbDate

mostRecent'' :: [DatabaseItem] -> UTCTime
mostRecent'' = foldr f minTime
    where minTime = UTCTime (toEnum 0) (secondsToDiffTime 0)
          f (DbDate t1) t2 | t1 < t2   = t2
                           | otherwise = t1

sumDb :: [DatabaseItem] -> Integer
sumDb = sum . filterDbNumber

sumDb' :: [DatabaseItem] -> Integer
sumDb' = foldr f 0
    where f (DbNumber n) sum = n + sum
          f _            sum = sum

avgDb :: [DatabaseItem] -> Double
avgDb xs = total numbers / size numbers
    where total = fromIntegral . sum
          size  = fromIntegral . length
          numbers = filterDbNumber xs

module Main where

import           Data.Maybe               (catMaybes)
import           HangmanLib
import           Test.Hspec
import           Test.QuickCheck
import           Test.QuickCheck.Function


prop_OnlyWordLetters :: Puzzle -> Bool
prop_OnlyWordLetters puzzle@(Puzzle word _ _) =
    guesses == word &&
    Nothing `notElem` correct &&
    catMaybes correct == word
    where (Puzzle _ correct guesses) = foldr (flip fillInCharacter) puzzle word

prop_OnlyNonWordLetters :: Puzzle -> Bool
prop_OnlyNonWordLetters puzzle@(Puzzle word _ _) =
    notIn word == guesses &&
    catMaybes correct == []
    where (Puzzle _ correct guesses) = foldr (flip fillInCharacter) puzzle $ notIn word
          notIn = take maxGuesses . nonWordLetters

nonWordLetters :: String -> String
nonWordLetters word = [c | c <- ['a'..'z'], c `notElem` word]

main :: IO ()
main = hspec $ do
    describe "fillInCharacter" $ do
        it "Selecting ONLY letters in word gives only correct guesses" $ do
           quickCheck prop_OnlyWordLetters
        it "Selecting No letters in word gives only incorrect guesses" $ do
           quickCheck prop_OnlyNonWordLetters
    let waltzing = freshPuzzle "waltzing"
    describe "handleGuess" $ do
        it "Correct letter is added to correct guesses and all guesses" $ do
            (Puzzle _ correct guesses) <- handleGuess waltzing 'a'
            'a' `elem` guesses && Just 'a' `elem` correct `shouldBe` True
        it "Incorrect letter is only added to all guesses" $ do
            (Puzzle _ correct guesses) <- handleGuess waltzing 'e'
            'e' `elem` guesses && (Just 'e') `notElem` correct `shouldBe` True
        it "The same guess is idempotent" $ do
            a@(Puzzle _ correct1 guessed1) <- handleGuess waltzing 'a'
            aa@(Puzzle _ correct2 guessed2) <- handleGuess a 'a'
            (Puzzle _ correct3 guessed3) <- handleGuess aa 'a'
            correct1 == correct2 &&
               guessed1 == guessed2 &&
               correct2 == correct3 &&
               guessed2 == guessed3 `shouldBe` True

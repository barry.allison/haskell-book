module Addition where
import           Test.Hspec
import           Test.QuickCheck

dividedBy :: Integral a => a -> a -> (a, a)
dividedBy num denom = go num denom 0
    where go n d count
              | n < d = (count, n)
              | otherwise = go (n - d) d (count + 1)

mul :: (Integral a) => a-> a -> a
mul a b = go a b 0
    where
        go _ 0 p = p
        go a b p = go a (b-1) (p+a)

genBool :: Gen Bool
genBool = choose (False, True)

genBool' :: Gen Bool
genBool' = elements [False, True]

genOrdering :: Gen Ordering
genOrdering = elements [LT, EQ, GT]

genChar :: Gen Char
genChar = elements ['a'..'z']

genTuple :: (Arbitrary a, Arbitrary b) => Gen (a, b)
genTuple = do
  a <- arbitrary
  b <- arbitrary
  return (a, b)

genTriple :: (Arbitrary a, Arbitrary b, Arbitrary c) => Gen (a, b, c)
genTriple = do
  a <- arbitrary
  b <- arbitrary
  c <- arbitrary
  return (a, b, c)

genEither :: (Arbitrary a, Arbitrary b) => Gen (Either a b)
genEither = do
  a <- arbitrary
  b <- arbitrary
  elements [Left a, Right b]

genMaybe :: Arbitrary a => Gen (Maybe a)
genMaybe = do
  a <- arbitrary
  elements [Nothing, Just a]

genMaybe' :: Arbitrary a => Gen (Maybe a)
genMaybe' = do
  a <- arbitrary
  frequency [ (1, return Nothing)
            , (3, return (Just a))]

main :: IO ()
main = hspec $ do
    describe "Addition" $ do
        it "1 + 1 is greater than 1" $ do
            ((1 + 1) :: Integer) > 1 `shouldBe` True
        it "2 + 2 is equal to 4" $ do
            ((2 + 2) :: Integer)  `shouldBe` 4
        it "27 dividedBy 9 is 3 remainder 0" $ do
            dividedBy 27 9 `shouldBe` (3, 0)
        it "22 dividedBy 5 is 4 remainder 2" $ do
            dividedBy 22 5 `shouldBe` (4, 2)
        it "mul 3 4 euals 12" $ do
            mul 3 4 `shouldBe` 12
        it "mul 0 10 euals 0" $ do
            mul 0 10 `shouldBe` 0
        it "mul 21 17 euals 357" $ do
            mul 21 17 `shouldBe` 357
        it "x + 1 us always greater than x" $ do
            property $ \x -> x + 1 > (x :: Int)

module ListOrderedTests where

import           Data.List       (sort)
import           Test.QuickCheck

listOrdered :: (Ord a) => [a] -> Bool
listOrdered xs = snd $ foldr go (Nothing, True) xs
    where go _ status@(_, False) = status
          go y (Nothing, t)      = (Just y, t)
          go y (Just x, t)       = (Just y, x >= y)

-- Not sure if the property is listOrdered True means that xs == sort xs or ...
propSort :: (Ord a) => [a] -> Bool
propSort xs | listOrdered xs = xs == sort xs
            | otherwise      = not (xs == sort xs)

-- ... if xs == sotr xs then listOrdered is True
propSort' :: (Ord a) => [a] -> Bool
propSort' xs | xs == sort xs = listOrdered xs
             | otherwise     = not $ listOrdered xs

main :: IO ()
main = do
  quickCheck (propSort :: String -> Bool)
  quickCheck (propSort' :: [String] -> Bool)
  quickCheck (propSort :: [Int] -> Bool)

module DivModQuotRemTests where

import           Test.QuickCheck

-- QuickCheck has a NonZero type
prop_DivMod :: Integral a => a -> (NonZero a) -> Bool
prop_DivMod x (NonZero y) =  (div  x y)*y + (mod x y) == x

prop_QuotRem :: Integral a => a -> (NonZero a) -> Bool
prop_QuotRem x (NonZero y) = (quot x y)*y + (rem x y) == x

main :: IO ()
main = do
  quickCheck (prop_DivMod :: Word -> NonZero Word -> Bool)
  quickCheck (prop_QuotRem :: Int -> NonZero Int -> Bool)
  quickCheck (prop_DivMod :: Integer -> NonZero Integer -> Bool)

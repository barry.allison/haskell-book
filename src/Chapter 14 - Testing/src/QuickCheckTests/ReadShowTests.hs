module TakeLengthTests where

import           Test.QuickCheck

prop_ReadShow :: (Eq a, Read a, Show a) => a -> Bool
prop_ReadShow x = (read (show x)) == x

prop_ReadShowDouble :: Double -> Bool
prop_ReadShowDouble = prop_ReadShow

main :: IO ()
main = do
  quickCheck prop_ReadShowDouble

module PowerTests where

import           Test.QuickCheck

mulAssociative :: (Eq a, Num a) => a -> a -> a -> Bool
mulAssociative x y z = x + (y + z) == (x + y) + z

mulCommutative :: (Eq a, Num a) => a -> a -> Bool
mulCommutative x y = x + y == y + x

mulIntAssociative :: Int -> Int -> Int -> Bool
mulIntAssociative = mulAssociative

mulDoubleCommutative :: Double -> Double -> Bool
mulDoubleCommutative = mulCommutative

main :: IO ()
main = do
  quickCheck mulIntAssociative
  quickCheck mulDoubleCommutative

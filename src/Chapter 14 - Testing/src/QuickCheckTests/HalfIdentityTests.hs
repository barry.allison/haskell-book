module HalfIdentityTests where

import           Test.QuickCheck

half :: Fractional a => a -> a
half x = x / 2

halfIdentity :: Fractional a => a -> a
halfIdentity = (*2) . half

propHalf :: (Eq a, Fractional a) => a -> Bool
propHalf x = halfIdentity x == x

main :: IO ()
main = do
  quickCheck (propHalf :: Float -> Bool)
  quickCheck (propHalf :: Double -> Bool)

module FunctionApplicationTests where

import           Test.QuickCheck
-- See:
-- https://www.reddit.com/r/HaskellBook/comments/8eeljk/chapter_14_testing_coarbitrary/
import           Test.QuickCheck.Function
-- See:
-- https://www.reddit.com/r/HaskellBook/comments/6yo7qb/ch_14_tips_how_to_prove_this_f_a_f_a/
import           Text.Show.Functions

-- The most generic functions
prop_Apply :: Eq b => (a -> b) -> a  -> Bool
prop_Apply f x = f x == (f $ x) -- why not? f x == f $ x ??

prop_Dollar :: Eq b => Fun a b -> a  -> Bool
prop_Dollar (Fn f) x = (f x) == (f $ x)

prop_Compose :: Eq c => (Fun b c) -> (Fun a b) -> a -> Bool
prop_Compose (Fn f) (Fn g) x = f (g x) == ((f . g) x)

-- Testable concrete functions

prop_IntString :: (Int -> String) -> Int -> Bool
prop_IntString = prop_Apply

prop_DoubleChar :: (Fun Double Char) -> Double -> Bool
prop_DoubleChar = prop_Dollar

prop_IntCharBool :: (Fun Char Bool) -> (Fun Int Char) -> Int -> Bool
prop_IntCharBool = prop_Compose

main :: IO ()
main = do
  quickCheck prop_IntString
  quickCheck prop_DoubleChar
  quickCheck prop_IntCharBool

module TakeLengthTests where

import           Test.QuickCheck

prop_TakeLength :: Int -> [a] -> Bool
prop_TakeLength n xs = length (take n xs) == n

prop_TakeStringLength :: Int -> [String] -> Bool
prop_TakeStringLength = prop_TakeLength

main :: IO ()
main = do
  quickCheck prop_TakeStringLength

module IdempotenceTests where

import           Data.Char
import           Data.List
import           Test.QuickCheck

capitaliseWord :: String -> String
capitaliseWord []     = []
capitaliseWord (x:xs) = toUpper x : xs

twice :: (a -> a) -> (a -> a)
twice f = f . f

fourTimes :: (a -> a) -> (a -> a)
fourTimes = twice . twice

prop_staysCapitalised :: String -> Bool
prop_staysCapitalised x = capitalised == twice capitaliseWord x &&
                     capitalised == fourTimes capitaliseWord x
    where capitalised = capitaliseWord x

prop_staysSorted :: Ord a => [a] -> Bool
prop_staysSorted xs = sorted == twice sort xs &&
                     sorted == fourTimes sort xs
    where sorted = sort xs

propSortedString :: String -> Bool
propSortedString = prop_staysSorted

propSortedInt :: [Int] -> Bool
propSortedInt = prop_staysSorted

main :: IO ()
main = do
  quickCheck prop_staysCapitalised
  quickCheck propSortedString
  quickCheck propSortedInt

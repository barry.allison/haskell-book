module ReverseTests where

import           Data.List       (reverse)
import           Test.QuickCheck

prop_Reverse :: Eq a => [a] -> Bool
prop_Reverse xs = id xs == (reverse . reverse) xs

main :: IO ()
main = do
  quickCheck (prop_Reverse :: [Int] -> Bool)
  quickCheck (prop_Reverse :: [String] -> Bool)

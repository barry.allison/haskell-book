module FoldEqualityTests where

import           Test.QuickCheck

prop_FoldAppend :: Eq a => [a] -> [a] -> Bool
prop_FoldAppend xs ys = foldr (:) xs ys == xs ++ ys

prop_FoldConcat :: Eq a => [[a]] -> Bool
prop_FoldConcat xss = foldr (++) [] xss == concat xss

prop_IntAppend :: [Int] -> [Int] -> Bool
prop_IntAppend = prop_FoldAppend

prop_StringConcat :: [String] -> Bool
prop_StringConcat = prop_FoldConcat


main :: IO ()
main = do
  quickCheck prop_IntAppend
  quickCheck prop_StringConcat

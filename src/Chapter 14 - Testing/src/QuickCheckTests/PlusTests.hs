module PlusTests where

import           Test.QuickCheck

plusAssociative :: (Eq a, Num a) => a -> a -> a -> Bool
plusAssociative x y z = x + (y + z) == (x + y) + z

plusCommutative :: (Eq a, Num a) => a -> a -> Bool
plusCommutative x y = x + y == y + x

plusIntegerAssociative :: Integer -> Integer -> Integer -> Bool
plusIntegerAssociative = plusAssociative

plusWordCommutative :: Word -> Word -> Bool
plusWordCommutative = plusCommutative

main :: IO ()
main = do
  quickCheck plusIntegerAssociative
  quickCheck plusWordCommutative

module SquareTests where

import           Test.QuickCheck

square :: Floating a => a -> a
square x = x * x

squareIdentity :: (Eq a, Floating a) => a -> Bool
squareIdentity x = (square . sqrt $ x) == x

squareDouble :: Double -> Bool
squareDouble = squareIdentity

squareFloat :: Float -> Bool
squareFloat = squareIdentity

main :: IO ()
main = do
  quickCheck squareDouble
  quickCheck squareFloat

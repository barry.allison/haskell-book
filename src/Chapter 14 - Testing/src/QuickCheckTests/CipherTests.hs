module CipherTests where

import           Data.Char       (chr, isLower, isUpper, ord)
import           Test.QuickCheck

-- Unicode aware
shiftRight' :: Int -> Char -> Char
shiftRight' n c
    | inRange c ('a', 'z') = wrapChar (ord 'a') n c
    | inRange c ('A', 'Z') = wrapChar (ord 'A') n c
    | otherwise = c
    where inRange x (lower, upper) = x >= lower&& x <= upper

caesar' :: String -> Int -> String
caesar' s n = map (shiftRight' n) s

unCaesar' :: String -> Int ->String
unCaesar' s n = caesar' s (26 - n)
----------------------------------------------------------------

shiftRight :: Int -> Char -> Char
shiftRight n c
    | isUpper c = wrapChar (ord 'A') n c
    | isLower c = wrapChar (ord 'a') n c
    | otherwise = c

wrapChar :: Int -> Int -> Char -> Char
wrapChar start offSet c = chr (start + (ord c + offSet - start) `mod` 26)

charShift :: Char -> Int
charShift c = ord c - ord 'A'

caesar :: String -> Int -> String
caesar s n = map (shiftRight n) s


unCaesar :: String -> Int ->String
unCaesar s n = caesar s (26 - n)


vigenere :: String -> String -> String
vigenere plain [] = plain
vigenere plain key = go plain (cycle key)
    where go [] _          = []
          go (' ':ps) key  = ' ' : go ps key
          go (p:ps) (k:ks) = shiftRight (charShift k) p : go ps ks

unVigenere :: String -> String -> String
unVigenere cipher [] = cipher
unVigenere cipher key = go cipher(cycle key)
    where go [] _          = []
          go (' ':ps) key  = ' ' : go ps key
          go (p:ps) (k:ks) = shiftRight (26 - (charShift k)) p : go ps ks

-- Arbitrary already has a Gen String instance and Gen doesn't work with type synonyms
-- instead of type CipherText = String, need a new type altogether
newtype CipherText = CipherText String deriving (Show)
-- vigenere keys must not be empty
newtype Key = Key String deriving (Show)

genChar :: Gen Char
genChar = elements $ ['a'..'z'] ++ ['A'..'Z']

genCipherText :: Gen CipherText
genCipherText = do
  s <- listOf genChar
  return $ CipherText s

genKey :: Gen Key
genKey = do
  s <- listOf1 genChar
  return $ Key s

instance Arbitrary CipherText where
    arbitrary = genCipherText

instance Arbitrary Key where
    arbitrary = genKey

prop_Caesar :: CipherText -> Int -> Bool
prop_Caesar (CipherText word) key = unCaesar (caesar word key) key == word

prop_Vigenere :: CipherText -> Key -> Bool
prop_Vigenere (CipherText word) (Key key) = unVigenere (vigenere word key) key == word

prop_CaesarUnicode :: String -> Int -> Bool
prop_CaesarUnicode word key = unCaesar' (caesar' word key) key == word

main :: IO ()
main = do
  quickCheck prop_CaesarUnicode
  quickCheck prop_Caesar
  quickCheck prop_Vigenere

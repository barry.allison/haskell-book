module ExponentialTests where

import           Test.QuickCheck

exponentialAssociative :: Integral a => a -> a -> a -> Bool
exponentialAssociative x y z = x ^ (y ^ z) == (x ^ y) ^ z

exponentialCommutative :: Integral a => a -> a -> Bool
exponentialCommutative x y = x ^ y == y ^ x

main :: IO ()
main = do
  quickCheck (exponentialAssociative :: Int -> Int -> Int -> Bool)
  quickCheck (exponentialCommutative :: Integer -> Integer -> Bool)

module ArbitraryFool where

import           Test.QuickCheck

data Fool =
    Fulse
  | Frue
    deriving (Eq, Show)

foolEqualGen :: Gen Fool
foolEqualGen = elements [Fulse, Frue]

foolFulsierGen :: Gen Fool
foolFulsierGen = frequency [(2, return Fulse), (1, return Frue)]

main :: IO ()
main = do
  putStrLn "Equal chances\n"
  sample foolEqualGen
  putStrLn "\nFulsier\n"
  sample foolFulsierGen

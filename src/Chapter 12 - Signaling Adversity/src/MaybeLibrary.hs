module MaybeLibrary where

isJust :: Maybe a -> Bool
isJust Nothing = False
isJust _       = True

isNothing :: Maybe a -> Bool
isNothing = not . isJust

mayybee :: b -> (a -> b) -> Maybe a -> b
mayybee b _ Nothing  = b
mayybee _ f (Just a) = f a

fromMaybe :: a -> Maybe a -> a
fromMaybe a = mayybee a id

listToMaybe :: [a] -> Maybe a
listToMaybe []    = Nothing
listToMaybe (a:_) = Just a

maybeToList :: Maybe a -> [a]
maybeToList Nothing  = []
maybeToList (Just a) = [a]

-- whenever you traverse a list applying function or logic, fold
catMaybes :: [Maybe a] -> [a]
catMaybes = foldr go []
    where go Nothing as  = as
          go (Just a) as = a:as

flipMaybe :: [Maybe a] -> Maybe [a]
flipMaybe = foldr go (Just [])
    where go Nothing _              = Nothing
          go _ Nothing              = Nothing
          go (Just a) (Just result) = Just (a:result)

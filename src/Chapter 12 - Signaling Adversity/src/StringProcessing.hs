module StringProcessing where

import           Data.Char (toLower)

notThe :: String -> Maybe String
notThe s = if (map toLower s) == "the"
           then Nothing
           else Just s

replaceThe :: String -> String
replaceThe = unwords .  map (nothingTo "a" . notThe) . words

nothingTo :: a -> Maybe a -> a
nothingTo x Nothing  = x
nothingTo _ (Just y) = y

countTheBeforeVowel :: String -> Integer
countTheBeforeVowel  = go . words
    where go []               = 0
          go ("the":(x:_):xs) = if isVowel x then 1 else 0 + go xs
          go (_:xs)           = go xs


isVowel :: Char -> Bool
isVowel c = toLower c `elem` vowels

countVowels :: String -> Integer
countVowels = toInteger . length . filter isVowel

newtype Word' =
    Word' String
    deriving (Eq, Show)

vowels = "aeiou"

mkWord :: String -> Maybe Word'
mkWord s = if numVowels > numConsonants
           then Nothing
           else Just $ Word' s
    where  numVowels = length $ filter isVowel s
           numConsonants = length s - numVowels

module TreeBuild  where

import           BinaryTree (BinaryTree (Leaf, Node))

unfold :: (a -> Maybe (a,b,a)) -> a -> BinaryTree b
unfold f a =
    case f a of
      Nothing           -> Leaf
      Just (la,  b, ra) -> Node (unfold f la) b (unfold f ra)

treeBuild :: Integer -> BinaryTree Integer
treeBuild n = unfold go 0
    where go d | d == n = Nothing
               | otherwise = Just (d+1, d ,d+1)

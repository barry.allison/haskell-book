module Natural where

-- As natural as any competitive bodybuilder
data Nat =
    Zero
  | Succ Nat
    deriving (Eq, Show)

natToInteger :: Nat -> Integer
natToInteger Zero     = 0
natToInteger (Succ x) = 1 + natToInteger x

integerToNat :: Integer -> Maybe Nat
integerToNat n | n < 0 = Nothing
               | otherwise = Just $ safeNat n

safeNat :: Integer -> Nat
safeNat 0 = Zero
safeNat n = Succ $ safeNat (n - 1)

module EitherLibrary where

lefts' :: [Either a b] -> [a]
lefts' = foldr go []
    where go (Left x) xs = x:xs
          go _        xs = xs

rights' :: [Either a b] -> [b]
rights' = foldr go []
    where go (Right x) xs = x:xs
          go _         xs = xs

partitionEithers' :: [Either a b] -> ([a], [b])
partitionEithers' = foldr go ([], [])
    where go (Left  l) (ls, rs) = (l:ls,   rs)
          go (Right r) (ls, rs) = (ls  , r:rs)

eitherMaybe' :: (b -> c) -> Either a b -> Maybe c
eitherMaybe' _ (Left  _) = Nothing
eitherMaybe' f (Right b) = Just (f b)

either' :: (a -> c) -> (b -> c) -> Either a b -> c
either' f _ (Left  a) = f a
either' _ f (Right b) = f b

eitherMaybe'' :: (b -> c) -> Either a b -> Maybe c
eitherMaybe'' f = either' (const Nothing) (Just . f)

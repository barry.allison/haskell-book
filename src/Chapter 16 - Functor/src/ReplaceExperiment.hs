module ReplaceExperiment where

import           Data.Char                      ( Char )

replaceWithP :: b -> Char
replaceWithP = const 'p'

lms :: [Maybe [Char]]
lms = [Just "Ave", Nothing, Just "woohoo"]

-- to match lms
replaceWithP' :: [Maybe [Char]] -> Char
replaceWithP' = replaceWithP

-- fmap replaceWithP :: Functor f => f a -> f Char
liftedReplace :: Functor f => f a -> f Char
liftedReplace = fmap replaceWithP

-- to match lms
liftedReplace' :: [Maybe [Char]] -> [Char]
liftedReplace' = liftedReplace


-- (fmap . fmap) replaceWithP :: (Functor f1, Functor f2) => f1 (f2 a) -> f1 (f2 Char)
twiceLifted :: (Functor f1, Functor f) => f (f1 a) -> f (f1 Char)
twiceLifted = (fmap . fmap) replaceWithP

-- for lms, f  ~ [],  f1 ~ Maybe
twiceLifted' :: [Maybe [Char]] -> [Maybe Char]
twiceLifted' = twiceLifted

thriceLifted :: (Functor f2, Functor f1, Functor f)
             => f (f1 (f2 a))
             -> f (f1 (f2 Char))
thriceLifted = (fmap . fmap . fmap) replaceWithP

-- for lms, f  ~ [],  f1 ~ Maybe
thriceLifted' :: [Maybe [Char]] -> [Maybe [Char]]
thriceLifted' = thriceLifted


e :: IO Integer
e =
    let ioi     = readIO "1" :: IO Integer
        changed = fmap read (fmap ("123" ++) (fmap show ioi))
    in  fmap (* 3) changed

f :: IO Integer
f =
    let ioi     = readIO "1" :: IO Integer
        changed = read <$> ("123" ++) <$> show <$> ioi
    in  (* 3) <$> changed


main :: IO ()
main = do
    putStr "replaceWithP' lms:   "
    print (replaceWithP' lms)
    putStr "liftedReplace lms:   "
    print (liftedReplace lms)
    putStr "liftedReplace' lms:  "
    print (liftedReplace' lms)
    putStr "twiceLifted lms:     "
    print (twiceLifted lms)
    putStr "twiceLifted' lms:    "
    print (twiceLifted' lms)
    putStr "thriceLifted lms:    "
    print (thriceLifted lms)
    putStr "thriceLifted' lms:   "
    print (thriceLifted' lms)

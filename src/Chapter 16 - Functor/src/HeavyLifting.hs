module HeavyLifting where

ioi :: IO Integer
ioi = readIO "1"

e :: IO Integer
e = let changed = fmap read (fmap ("123"++) (fmap show ioi))
    in fmap (*3) changed

f :: IO Integer
f = let changed = read <$> ("123"++) <$> show <$> ioi
    in (*3) <$> changed

g :: IO Integer
g = let changed = (read . ("123"++) . show) <$> ioi
    in (*3) <$> changed

h :: IO Integer
h = (*3) <$> read <$> ("123"++) <$> show <$> ioi

i :: IO Integer
i = ((3*) . read . ("123"++) . show) <$> ioi
